let
  nixpkgs = import ./nixpkgs.nix;
  pkgs = import nixpkgs {
    system = "x86_64-linux";
  };

in
pkgs.mkShell {
  buildInputs = with pkgs; [
    shadow
    nix
    nano
    yarn
    yarn2nix
  ];
  shellHook = ''
    export NIX_PATH="nixpkgs=${nixpkgs}"
    export NIX_BUILD_CORES=4


    echo -e "The following commands are available\n"
    yarn2nix > yarn.nix
  '';
}

