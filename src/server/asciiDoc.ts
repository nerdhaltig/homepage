
// tag::init[]
import Processor from 'asciidoctor'
const prismExtension = require('asciidoctor-prism-extension');
const processor = Processor()
// end::init[]


// tag::syntaxhighlight[]
processor.SyntaxHighlighter.register('prism', prismExtension);
// end::syntaxhighlight[]

// tag::read[]

import { readFile } from "fs/promises";
import path from 'path'
import { AdocMetadata } from '../functions/fileMetadata'


let basePath: string = "./src"



// will read metadata and htmlcontent of an asciidoc and return both
export async function ReadAsciidoc(
    relPath: string,
    filename: string,
    attributes: string[]
): Promise<[AdocMetadata | undefined, string | undefined]> {

    // we only accept adoc-files
    if (!filename.includes(".adoc")) {
        return [undefined, undefined];
    }

    // we read from file
    let fileContent = await readFile(basePath + relPath + "/" + filename)
    console.debug("[ReadAsciidoc]", `try to load asciidoc ${basePath + relPath + "/" + filename}`)


    // we parse the document
    let adoc = processor.load(fileContent, {
        doctype: "book",
        attributes: {
            'source-highlighter': 'prism',
        },
    })


    let newMetadata: any | undefined = {}

    for (let attribute of attributes) {
        switch (attribute) {
            case "path":
                newMetadata.path = relPath
                break
            case "filename":
                newMetadata.filename = path.parse(filename).name
                break
            case "title":
                newMetadata.title = adoc.getAttribute("doctitle", "")
                break
            case "date":
                newMetadata.date = new Date(adoc.getAttribute("date", "")).getTime();
                break
            case "tags":
                newMetadata.tags = (adoc.getAttribute("tags", "") as string).split(" ")
                break
            case "cards":
                newMetadata.cards = (adoc.getAttribute("cards", "") as string).split(" ")
                break
            default:
                newMetadata[attribute] = adoc.getAttribute(attribute, "")
                break
        }
    }

    console.error("[ReadAsciidoc]", "parsed metadata", newMetadata)

    // convert doc to html
    let adocContent: string | undefined = undefined
    let adocConverted = processor.convert(fileContent, {
        doctype: "book",
        attributes: {
            'source-highlighter': 'prism',
            'prism-theme': 'atom-dark',
            'tip-caption': 'bulb',
            'note-caption': '💁',
            'important-caption': 'heavy_exclamation_mark',
            'caution-caption': 'fire',
            'warning-caption': '🚧 WARNUNG 🚧',
        },
        backend: 'html5',
        safe: 'unsafe',
        base_dir: basePath + relPath,
    })
    console.error("[ReadAsciidoc]", `convert to html in folder '${basePath + relPath}' `)

    if (typeof adocConverted === "string") {
        adocContent = adocConverted
    } else {
        console.error("[ReadAsciidoc]", "adoc is not a string", adocConverted)
    }


    return [newMetadata, adocContent]
}
// end::read[]




/*
const loaderUtils = require("loader-utils"),
    asciidoctor = require("asciidoctor")(),
    asciidoctorHtml5s = require("asciidoctor-html5s"),
    jsdom = require("jsdom"),
    highlightJsExt = require('asciidoctor-highlight.js')


// Register the HTML5s converter and supporting extension.
asciidoctorHtml5s.register()
// Register server-side hightlightjs highlighting
highlightJsExt.register(asciidoctor.Extensions)

// default option
const defaultOptions = {
    safe: "unsafe",
    backend: "html5s"
}

const loggerManager = asciidoctor.LoggerManager
const memoryLogger = asciidoctor.MemoryLogger.create()
loggerManager.setLogger(memoryLogger)

memoryLogger.getMessages() // returns an array of Message
module.exports = function (content) {
    this.cacheable && this.cacheable(true)
    var params = loaderUtils.getOptions(this)
    var options = Object.assign({}, defaultOptions, params)

    var result = asciidoctor.convert(content, options)

    memoryLogger.getMessages().forEach(message => {
        if (message.getText().match(/no callout found for/)) {
            // suppress these warnings; as a workaround for https://github.com/asciidoctor/asciidoctor/issues/3359
            return;
        }
        if (message.getSeverity() === 'DEBUG') {
            return;
        }
        console.log(message.getSeverity() + ": " + message.getText())
        const sourceLocation = message.getSourceLocation()
        if (sourceLocation) {
            console.log(sourceLocation.getLineNumber())
            console.log(sourceLocation.getFile())
            console.log(sourceLocation.getDirectory())
            console.log(sourceLocation.getPath())
        }
    })

    // parse HTML including normalizing broken HTML
    const doc = new jsdom.JSDOM("<html><body>" + result + "</body><html>").window.document;

    // minimize HTML to optimize it for Bulma 

    // remove sections wrapper
    doc.querySelectorAll("section").forEach(element => {
        element.replaceWith(...element.childNodes)
    })

    // remove div.ulist wrapper
    doc.querySelectorAll("div.ulist").forEach(element => {
        element.replaceWith(...element.childNodes)
    })

    // remove div.olist wrapper
    doc.querySelectorAll("div.olist").forEach(element => {
        element.replaceWith(...element.childNodes)
    })

    // remove empty toc-title
    doc.querySelectorAll("h2#toc-title").forEach(element => {
        if (element.textContent === "") {
            element.remove()
        }
    })

    // remove empty toc from Google's summary
    // https://developers.google.com/search/docs/advanced/crawling/special-tags
    doc.querySelectorAll("#toc").forEach(element => {
        element.setAttribute("data-nosnippet", "")
        element.setAttribute("role", "navigation")
        element.setAttribute("aria-label", "Blog post")
    })

    // rewrite callouts in listings so that copy&paste won't copy callouts
    doc.querySelectorAll("b.conum").forEach(element => {
        element.setAttribute("data-value", element.textContent)
        element.textContent = ""
    })

    result = doc.querySelector("body").innerHTML;

    return result
}
*/