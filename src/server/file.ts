

import fs from 'fs';
import { writeFile } from 'fs/promises';
import path from 'path';

// tag::files[]
// will return an fs.Dirent array of an folder
export async function GetFilesInFolder(path: string): Promise<string[]> {

    return new Promise<string[]>((resolve, reject) => {
        fs.readdir(path, { withFileTypes: true }, async (err: NodeJS.ErrnoException | null, files: fs.Dirent[]) => {
            if (err === null) {
                let filenames: string[] = []
                for (let file of files) {
                    if (file.isFile()) {
                        filenames.push(file.name)
                    }
                }

                console.debug("[GetFilesInFolder]", path, filenames)
                resolve(filenames)
            } else {
                console.debug("[GetFilesInFolder]", path, [])
                resolve([])
            }
        })
    })
}
// end::files[]

// will save filecontent to an filename and create folders if needed
// this function will not save the file, if the content is equal
// this prevents retrigger of some dev-servers
export async function SaveToFile(filename: string, filecontent: string) {
    if (filename === "") {
        console.error("filename is empty")
        return
    }


    let targetPath = path.parse(filename).dir;


    // read file
    fs.readFile(filename, async (err: NodeJS.ErrnoException | null, data: Buffer) => {

        if (err === null) {
            if (data.toString() === filecontent) {
                console.debug("[SaveToFile]", filename, "file-content is equal, not storing it")
                return
            }
        }

        if (!fs.existsSync(targetPath)) {
            console.debug("[SaveToFile]", filename, "create folder", targetPath);
            fs.mkdirSync(targetPath, { recursive: true });
        }

        console.debug("[SaveToFile]", filename, "write to file");
        await writeFile(filename, filecontent);
    })


}
