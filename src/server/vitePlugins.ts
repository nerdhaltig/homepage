// tag::plugin[]

import path from "path";
import { Plugin, ViteDevServer } from "vite";
import { GetFilesInFolder, SaveToFile } from "./file";
import { ReadAsciidoc } from "./asciiDoc";
import { AdocMetadata } from "~/functions/fileMetadata";

// options for the plugin
interface Options {}

export function GetHtmlFromAsciiDoc(options?: Options): Plugin {
  return {
    configureServer: async (server: ViteDevServer) => {
      server.middlewares.use(async (req, res, next) => {
        if (req.originalUrl?.includes("/adocs.json", 0)) {
          let targetPath = path.parse(req.originalUrl).dir;
          let metadataTags: string[] = [
            "path",
            "filename",
            "date",
            "title",
            "titleimage",
            "color",
            "icon",
            "description",
            "cardsfolder",
            "cards",
            "tags",
            "recursive",
            "state",
            "statecolor",
            "draft",
          ];
          let metadatas: AdocMetadata[] = [];

          let filenames = await GetFilesInFolder("./src" + targetPath);
          for (let filename of filenames) {
            // load metadata and document-content
            let newMetadata: AdocMetadata | undefined;
            let newDocContent: string | undefined;
            [newMetadata, newDocContent] = await ReadAsciidoc(
              targetPath,
              filename,
              metadataTags
            );
            if (newMetadata === undefined || newDocContent === undefined) {
              continue;
            }

            // validate date
            if (newMetadata.draft !== undefined) {
              if (newMetadata.draft === "") {
                metadatas.push(newMetadata);
              }
            } else {
              metadatas.push(newMetadata);
            }

            // save metadata
            await SaveToFile(
              "./public" +
                newMetadata.path +
                "/" +
                newMetadata.filename +
                ".json",
              JSON.stringify(newMetadata)
            );
            // save content
            await SaveToFile(
              "./public" +
                newMetadata.path +
                "/" +
                newMetadata.filename +
                ".html",
              newDocContent
            );
          }

          // sort it
          metadatas.sort(function (a, b) {
            if (a === undefined || b === undefined) return 0;
            if (
              (a as AdocMetadata).date !== undefined &&
              (b as AdocMetadata).date !== undefined
            ) {
              // Turn your strings into dates, and then subtract them
              // to get a value that is either negative, positive, or zero.
              let result = (b as any).date - (a as any).date;
              return result;
            }

            if (a > b) {
              return -1;
            }
            if (b > a) {
              return 1;
            }
            return 0;
          });

          await SaveToFile(
            "./public" + req.originalUrl,
            JSON.stringify(metadatas)
          );

          res.end(JSON.stringify(metadatas));
          return;
        }

        next();
      });
    },
  } as Plugin;
}

// end::plugin[]
