import axios, { AxiosResponse } from "axios"
import { AdocMetadata } from "./fileMetadata"

export async function GetListOfAsciiDocs(folder: string): Promise<AdocMetadata[]> {
    return new Promise<AdocMetadata[]>((resolve, reject) => {

        let response = axios.get(folder + "/adocs.json")
        response.then((response: AxiosResponse<any, any>) => {

            if (response === undefined) {
                console.error("[GetListOfAsciiDocs]", "response is undefined")
                resolve([])
                return
            }
            console.debug("[GetRouteFromMetadata]", response.data)
            let metadatas = response.data as AdocMetadata[]

            resolve(metadatas)
            return
        })

    })
}



export async function LoadAsciiDoc(filename: string): Promise<[AdocMetadata | undefined, string | undefined]> {

    let adocFilename = filename.replace(".adoc", ".html")
    let adocFilenameMetadata = filename.replace(".adoc", ".json")
    let adocContent: string | undefined = ""
    let adocMetadata: AdocMetadata | undefined = {} as AdocMetadata

    let rspContent = await axios.get(adocFilename)
    if (rspContent !== undefined) {
        if (rspContent.status === 200) {
            adocContent = rspContent.data
        }
    }

    let rspMetadata = await axios.get(adocFilenameMetadata)
    if (rspMetadata !== undefined) {
        if (rspMetadata.status === 200) {
            adocMetadata = rspMetadata.data
        }
    }

    return [adocMetadata, adocContent]
}
