export interface AdocMetadata {
  path: string | undefined;
  filename: string | undefined;
  date: number | undefined;
  title: string | undefined;
  titleimage: string | undefined;
  color: string | undefined;
  icon: string | undefined;
  description: string | undefined;
  cardsfolder: string | undefined;
  cards: string[] | undefined;
  tags: string[] | undefined;
  recursive: string | undefined;
  state: string | undefined;
  statecolor: string | undefined;
  draft: string | undefined;
}

export interface AdocMetadatas {
  [name: string]: AdocMetadata[];
}
