import axios, { AxiosResponse } from "axios";
import { AdocMetadata } from "./fileMetadata";


export class ADocs {
    private cards: Map<string, AdocMetadata> = new Map()

    public async Load(path: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            let response = axios.get(`${path}/adocs.json`)
            response.then((response: AxiosResponse<any, any>) => {
                for (let cardMetadata of (response.data as AdocMetadata[])) {
                    if (cardMetadata.filename !== undefined) {
                        this.cards.set(cardMetadata.filename, cardMetadata)
                    }
                }
                resolve()
            })
        })
    }

    public IDs(): string[] {
        return Array.from(this.cards.keys())
    }

    public List(cardNames: string[] | undefined): AdocMetadata[] {

        var metadatas: AdocMetadata[] = []
        if (cardNames === undefined) {
            return metadatas
        }

        for (let cardName of cardNames) {
            let metadata = this.cards.get(cardName)
            if (metadata !== undefined) {
                metadatas.push(metadata)
            }
        }

        return metadatas
    }
}

export class ADocsFolders {
    private folders: Map<string, ADocs> = new Map()

    async Get(folder: string): Promise<ADocs> {

        let cardList = this.folders.get(folder)
        if (cardList === undefined) {
            cardList = new ADocs()
            await cardList.Load(folder)
            this.folders.set(folder, cardList)
        }

        return cardList
    }

}

export const folders: ADocsFolders = new ADocsFolders()
