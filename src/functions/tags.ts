import axios, { AxiosResponse } from "axios"


export interface PageTag {
    id: string,
    color: string,
    icon: string,
    label: string,
}

var tags: PageTag[] = []

export async function LoadAllTags() {
    return new Promise<void>((resolve, reject) => {
        let response = axios.get(`/atags/adocs.json`)
        response.then((response: AxiosResponse<any, any>) => {
            tags = response.data
            resolve()
        })
    })
}

export function GetTags(tagIDs: string[]): PageTag[] {
    let newPageTags: PageTag[] = []
    for (let tagID of tagIDs) {
        for (let pageTag of tags) {
            if (pageTag.id === tagID) {
                newPageTags.push(pageTag)
            }
        }
    }

    console.debug(newPageTags)
    return newPageTags
}

export function GetAllTagIDs(): string[] {
    let allTagIDs: string[] = []
    for (let tag of tags) {
        allTagIDs.push(tag.id)
    }

    return allTagIDs
}