---
id: "nerdhaltig.md"
date: "2021-10-01T18:27:20.275Z"
title: "This Homepage"
image: "/img/under-construction1.png"
description: "Software ist nie fertig.. Webseiten auch nicht 😉 und bevor ich diese Seite nie veröffentliche, weil ich nie damit zufrieden bin, schalte ich Sie jetzt einfach mal online."
tags:
  - programming
---

<img class="q-img__image q-img__image--with-transition q-img__image--loaded" style="object-fit: cover; object-position: 50% 50%; height:250px" src="/img/vscode_nerdhaltig.png" alt="vscode_nerdhaltig"  />

## Bootstrap

Dies Homepage ist eine Standard vue3-app erstellt mit.

```javascript
yarn create vite nerdhaltic --template vue-ts
yarn add @types/node
```

## Features
* 🏗 build: [vite](https://vitejs.dev/guide/#scaffolding-your-first-vite-project)
* 💄 ui [quasar](https://quasar.dev/start/vite-plugin)
* router [vue-router](https://next.router.vuejs.org/installation.html)
* autoimport of components [unplugin-vue-components]
* import md as components [vite-plugin-md]

### Autoimport

Ein Feature aus nuxt habe ich 🧡 gelernt: Der autoimport von Komponenten. Das ist ein cooles feature und spart Schreibarbeit.

Hierfür gibt es ein Plugin für Vite: [unplugin-vue-components](https://github.com/antfu/unplugin-vue-components)

Einfach wie auf der GitHub-Seite beschrieben installieren ( für yarn: `yarn add unplugin-vue-components -D` ) 

Meine config sieht folgendermaßen aus:

```javascript
import Components from 'unplugin-vue-components/vite' // autoimport of components


Components({
      extensions: ['vue', 'md'],

      // allow auto import and register components used in markdown
      include: [/\.vue$/, /\.vue\?vue/],

      dirs: ['src/components', 'src/pages'],
      deep: true,
      directoryAsNamespace: true,

      dts: 'src/components.d.ts',
    })
```

Ein paar Anmerkungen:

- sollte mal nicht klar sein, wie eine Komponente heißt, dann kann man in der Datei `src/components.d.ts` nach sehen.


### Markdown

Um markdown in vue3 zu nutzen, wird hier auch ein vite-plugin genutzt, welches Markdown-files in Komponenten umwandelt.

Hier sieht die config folgendermaßen aus:

```javascript
import Markdown from 'vite-plugin-md' // Markdown to Components
import MarkdownImageSize from 'markdown-it-imsize'

// convert md to component`
    Markdown({
      markdownItSetup(md) {
        // for example
        md.use(MarkdownImageSize)
      },
    }),
```

### Syntax highlighting

Es wird bereits `vite-plugin-md` genutzt, unter der Haube wird dort markdown-it verwendet. Das ist gut, denn dann können wir die ganzen Plugins von markdown-it verwenden.

So auch das plugin `markdown-it-prism` welches uns Syntax highlighting auf diese Seite bring

```javascript

import MarkDownPrism from 'markdown-it-prism';

...

export default defineConfig({
  plugins: [

    // convert md to components
    Markdown({

      markdownItSetup(md) {
        ...
        md.use(MarkDownPrism, {});
      },
    }),
  ],
```

## Autoindex

Okay, makdown files können als Komponenten genutzt werden, jetzt fehlt nur noch der Aufruf jener.
Hierfür braucht man den exakten Pfad zum Mardown-File damit dies funzt, auch will ich ja eine liste der Blog-Einträge anzeigen,
somit brauch ich eine liste alle verfügbaren einträge.

Früher (tm) hab ich die per Hand geschrieben, das war mühsam und Fehleranfällig. Aktuell nutze ich ein selbst-geschriebenes `vite-plugin` 

Der trick ist, eine `entrys.json` ab zu fragen, dies wird von dem plugin abgefangen und dann die entrys.json erzeugt. Das funktioniert leider nur wenn der dev server läuft. Im Produktivbetrieb wird die datei von den assets abgefragt.


### Kopieren der automatisch erzeugten json-files

Die automatisch-erzeugten json-files müssen noch für den finalen build in den asset-ordner kopiert werden. Herfür für das plugin `rollup-plugin-copy` verwendet.

```bash
yarn add rollup-plugin-copy -D
```

## Build

### Nix-Build

Diese Seite läuft unter [NixOS](https://nixos.org/) die nix-expressions liegen im `./build/nix`-Order.

Es kann aber auch aus dem repo heraus ein build durchgeführt werden um zu testen ob alles funzt.

`nix-build -A local --no-out-link`

## Benutzen

### Nix-Module
Ein NixOS-Modul wird hier bereit gestellt um die Webseite einfacher in NixOS ein zu binden.

Das Modul kann folgendermaßen eingebunden werden:

```javascript
{ config, pkgs, lib, ... }:
let

  src = builtins.fetchGit {
    url = "https://gitlab.com/nerdhaltig/homepage.git";
    ref = "main";
    rev = "fab4a9be0b8b48d0818ed83635dd09be189e9098";
  };
  nerdhaltig = import (src) { };

in {

  imports = [ nerdhaltig.module ];

  services.homepage-nerdhaltig = {
    enable = true;
    package = nerdhaltig.public;
  };

}
```

Dies aktiviert nerdhaltig.de mit nginx und traefik als proxy.

## Deployment mit gitlab

Das deployment erfolgt nicht in dem [Homepage-Repo](https://gitlab.com/nerdhaltig/homepage),
sondern aus dem repo welches die nix-expressions für den [nerdhaltig-server](https://gitlab.com/nerdhaltig/server) beinhaltet

### Vorraussetzungen

In GitLab müssen folgende Variablen vorhanden sein:
- sshkey
- SSH_KNOWN_HOSTS

#### sshkey
Der SSH-Key muss in gitlab als base64 vorliegen, damit dieser durch gitlab "maskiert" werden kann sollte er in logs auftauchen.
erstellt werden kann es `cat sshkey.private | base64 -w 0`

#### SSH_KNOWN_HOSTS
Diese Variable muss nicht maskiert werden ( sind eh öffentliche Infos ). Eingetragen wird hier der Inhalt von known_hosts.


### Skripte

Während des deployments muss in der nix-expression der git-hash angepasst werden, damit die aktuelle Version gebaut wird. Dies erledigt der Skript `deployment/prepare.bash` welcher von der `.gitlab-ci.yml` aufgerufen wird.

Danach wird `deployment/deploy.sh` ausgeführt, welches die nix-expressions auf den server pushed und `nixos-rebuild switch` ausführt.

