= Filament-Temperatur
:date: 2022-08-07T11:25:00.000Z
:titleimage: /img/3DDruck/petg_midori_temp_230-250.jpg
:description: Die Filamenttemperatur zu prüfen ist immer eine gute Idee. Nicht immer stimmen die Herstellerangaben und es gibt auch Abweichungen beim Temperatursensor.
:tags: 3ddruck

== Testen von Filamenten


=== Aus dem Leben

Manchmal bekommt man bei Filamenten wiedersprüchliche Angaben. Ein Beispiel aus einem shop: 

image::/img/3DDruck/petg_filament_detail.jpg[]

und dann klickt man sich durch die Bilder des Filaments und bekommt folgende Angaben ...

image::/img/3DDruck/petg_filament_picture.jpg[]

okay, was ist jetzt hier die richtige Angabe ? ( Spoiler: das Bild )

=== Ausprobieren

Eine gute Idee ist es, eine neue Filamentrolle von einem Hersteller mal zu testen. Hierfür verwende ich meinen Kalibrierwürfel, fange bei einer Grund-Temperatur an und erhöhe nach 30% die Temperatur.

.Zu wenig Temperatur.. und es löst sich vom Druckbett
image::/img/3DDruck/petg_midori_temp_190-200.jpg[height=480]

.In diesem Bereich schaut der Druck ganz gut aus
image::/img/3DDruck/petg_midori_temp_215-235.jpg[height=480]

.Ab 240° wird die Oberfläche rau.. zu viel Temperatur für dieses Filament
image::/img/3DDruck/petg_midori_temp_230-250.jpg[height=480]

.Auch bei zu viel Temperatur, entstehen Fäden und die Oberfläche sieht nicht so gut aus
image::/img/3DDruck/petg_midori_oberteil.jpg[]

.
