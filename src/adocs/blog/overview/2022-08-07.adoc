= Umbau auf AsciiDoc
:date: 2022-08-07T11:55:00.000Z
:titleimage: /img/under-construction1.png
:description: Um besser Content zusammen zu bauen, hab ich diese Seite auf AsciiDoc umgebaut
:tags: nerdhaltig programming vue
:prism-languages: bash,haskell,typescript

ASCIIDOC hat einige Vorteile 🫶

- Schnippsel aus anderen Dateien importieren
- Code auf vorhandenen Dateien importieren
** somit ist beispielcode immer aktuell
- aufteilen der Artikel/Dokumentation
** somit keine doppelte Schreibarbeit mehr 🚮


Ein gutes Beispiel ist dieser Blog-Post. 

Es ist einmal dieser Post, der drauf hin weißt, dass die Seite jetzt mit AsciiDoc gebaut wird, anderer Seites gibt es hier ja die "Projektseite" wie auch diese Page als Projekt hinterlegt ist.

Und dank asciidoc muss ich nix kopieren, sondern nur einbinden 🚀

== AsciiDoc in VueJS

include::../../projects/active/nerdhaltig/asciidoc.adoc[]