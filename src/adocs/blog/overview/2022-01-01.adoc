= Happy New Year - With a new site
:date: 2022-01-01T00:00:00.000Z
:titleimage: /img/fireworks-4021214_1280.jpg
:description: Frohe neue Seite
:tags: vue nerdhaltig

== TLDR

Neue Seite, vue3, vite, quasi. Ja, klar soweit ? 😼

== Happy new year

Auf das dieses neue Jahr mehr Segelerfahrung bringt und ein paar Projekte fertig werden 🤭

( mehr Infos auf der Projekt-Seite )