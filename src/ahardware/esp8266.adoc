= ESP8266
:date: 2022-07-29T20:40:20.275Z
:titleimage: /img/esp8266.jpg
:description: WiFi-Modul mit UART / I2C / PWM / GPIO
:tags: hardware microcontroller wifi
:cost: 12,70

Der ESP8266 ist sehr beliebt bei Projekten die WiFi benötigen.

Kann von hier bezogen werden:

- https://www.reichelt.de/nodemcu-esp8266-wifi-modul-debo-jt-esp8266-p219900.html?&trstct=pos_0&nbc=1[Reichelt - ESP8266]

