import { createApp } from "vue";
import App from "./App.vue";

const myApp = createApp(App);

// ############################### Quasar ###############################
import { Quasar } from "quasar";
import quasarLang from "quasar/lang/de";

// Import Quasar css
import "quasar/src/css/index.sass";

myApp.use(Quasar, {
  plugins: {}, // import Quasar plugins and add here
  lang: quasarLang,
});

// ############################### Syntax highlight ###############################
//import 'prismjs/themes/prism-tomorrow.css'
//import 'prism-themes/themes/prism-atom-dark.css'
import "./css/admonitionblock.css";
import "prism-themes/themes/prism-vsc-dark-plus.min.css";
//import "@asciidoctor/core/dist/css/asciidoctor.css";
import "./css/asciidoctor.css";

// ############################### Router ###############################
import { createRouter, createWebHashHistory } from "vue-router";

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: `/`,
      redirect: "/adocs/blog/overview/list",
    },
    {
      name: "list",
      path: "/:path(.*)*/list",
      component: () => import("./components/documentList.vue"),
    },
    {
      name: "content",
      path: "/:path(.*)*/content",
      component: () => import("./components/content.vue"),
    },
  ],
});

// Make sure to _use_ the router instance to make the
// whole app router-aware.
myApp.use(router);

myApp.mount("#app");
