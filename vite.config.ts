import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'
import path from 'path'


import Components from 'unplugin-vue-components/vite' // autoimport of components

import { GetHtmlFromAsciiDoc } from './src/server/vitePlugins'

// https://vitejs.dev/config/
export default defineConfig({



  plugins: [

    vue({
      include: [/\.vue$/],
      template: { transformAssetUrls }
    }),

    quasar({
      sassVariables: 'src/quasar-variables.sass'
    }),

    // auto-import components
    Components({
      extensions: ['vue'],

      // allow auto import and register components used in markdown
      include: [/\.vue$/, /\.vue\?vue/],

      dirs: ['src/components', 'src/pages'],
      deep: true,
      directoryAsNamespace: true,

      dts: 'src/components.d.ts',
    }),

    GetHtmlFromAsciiDoc(),
  ],

  resolve: {
    alias: {
      '~': path.resolve(__dirname, './src'),
      '@': path.resolve(__dirname, './src'),
      '@components': path.resolve(__dirname, './src/components'),
      '@plugins': path.resolve(__dirname, './src/plugins'),
    },
  },


  build: {
    rollupOptions: {
      output: {
        manualChunks(id) {
          if (id.includes('node_modules/vue3-markdown-it')) {
            return 'markdown-it';
          }
          if (id.includes('node_modules')) {
            return 'modules';
          }
        }

      },
    }
  }


})
