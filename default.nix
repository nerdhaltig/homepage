# nix-build -E 'with import <nixpkgs> { }; callPackage ./default.nix { packageVersion = "latest"; }'
{ system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
, packageVersion ? "develop"
}:
let
  lib = pkgs.lib;
  callPackage = lib.callPackageWith (pkgs // pkgs.lib);
  inherit (lib) sourceByRegex;
  inherit (pkgs) mkYarnPackage;

in
mkYarnPackage {
  name = "nerdhaltig-${packageVersion}";
  src = sourceByRegex ./. [
    "^package.json"
    "^yarn.lock"
    "src"
    "src/.*"
    "public"
    "public/.*"
    "index.html"
    "^tsconfig.json"
    "^vite.config.ts"
  ];
  version = packageVersion;

  yarnNix = ./yarn.nix;
  yarnLock = ./yarn.lock;
  packageJSON = ./package.json;

  patches = [ ];

  buildPhase = ''
    echo -e "export const version = \"${packageVersion}\"" > deps/nerdhaltig.de/src/version.ts
    yarn --offline build
  '';

  distPhase = "true";

  installPhase = ''
    mkdir -p $out
    cp -vR deps/nerdhaltig.de/dist/* $out/
  '';
}

