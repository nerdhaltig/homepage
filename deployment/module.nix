{ config, pkgs, lib, ... }:
let
  inherit (lib) mkDefault mkEnableOption mkForce mkIf mkMerge mkOption types;
  inherit (lib)
    mapAttrs mapAttrs' mapAttrsToList nameValuePair optional optionalAttrs
    optionalString;

  hps = config.services.homepages;

  homepageOptions = { ... }: {

    options = {

      enable = mkEnableOption "Homepage served with traefik and nginx";

      package = mkOption {
        defaultText = "package";
        description = "Which derivation to use. The derivation should contain your index.html";
        type = types.package;
      };

      domain = mkOption {
        default = "example.com";
        description =
          "The domain where your homepage is running on, please omit www.";
        type = types.str;
      };

      fqdn = mkOption {
        default = "www.example.com";
        description =
          "the fqdn";
        type = types.str;
      };

      host = mkOption {
        default = "127.0.0.1";
        description = "The address where nginx is listening on";
        type = types.str;
      };

      port = mkOption {
        default = 7878;
        description = "The port where this page listening";
        type = types.port;
      };

      traefik = mkOption {
        default = true;
        description = "Enable traefik as entry-point-proxy";
        type = types.bool;
      };

      letsencrypt = mkOption {
        default = true;
        description = "Use letsencrypt to get an ssl-cert. Only works with traefik-enabled";
        type = types.bool;
      };

      dnsmasq = mkOption {
        default = false;
        description = "This will add your homepage to dnsmasq, for development";
        type = types.bool;
      };


    };
  };

  mkServiceNginx = name: value: nameValuePair ("hp-" + name) (
    {
      listen = [{
        addr = value.host;
        port = value.port;
      }];

      locations = {
        "/" = {
          root = "${value.package}";
          extraConfig = ''
            index index.html;
            autoindex off;
            add_header Cache-Control "no-store";
            try_files $uri $uri/index.html /index.html;
          '';
        };
      };
    });




  mkTraefikRoute = name: value: nameValuePair ("hp-route-" + name) (
    mkIf value.traefik {
      entrypoints = "https";
      tls = mkIf value.letsencrypt { certresolver = "letsencrypt"; };
      rule = "Host(`${value.domain}`) || Host(`${value.fqdn}`)";
      middlewares = "hp-middleware-${name}";
      service = "hp-service-${name}";
    }
  );

  mkTraefikMiddleware = name: value: nameValuePair ("hp-middleware-" + name) (
    mkIf value.traefik {
      redirectregex = {
        regex = "^(?:https?)://${value.domain}";
        replacement = "https://${value.fqdn}";
      };
    }
  );

  mkTraefikServices = name: value: nameValuePair ("hp-service-" + name) (
    mkIf value.traefik {
      loadBalancer = {
        servers =
          [{ url = "http://" + value.host + ":" + toString value.port; }];
      };
    }
  );


in
{

  ###### interface
  options = {
    services.homepages = mkOption {
      type = types.attrsOf (types.submodule homepageOptions);
      default = { };
      description = "One or more homepages";
    };
  };

  ###### implementation

  ###### implementation
  config = mkIf (hps != { }) {
    services.nginx = {
      virtualHosts = mapAttrs' mkServiceNginx hps;
    };

    services.traefik = {
      dynamicConfigOptions = {
        http = {
          routers = mapAttrs' mkTraefikRoute hps;
          middlewares = mapAttrs' mkTraefikMiddleware hps;
          services = mapAttrs' mkTraefikServices hps;
        };
      };
    };

    #services.traefik = mapAttrs' mkServiceTraefik homepages;
    services.dnsmasq = {
      extraConfig = lib.strings.concatMapStrings (domain: "address=/${domain}/127.0.0.1\n")
        (
          # get an array of domains
          lib.attrsets.catAttrs "domain"
            (
              # convert set to an array
              lib.attrsets.attrValues
                (
                  # only if dnsmasq is enabled
                  lib.attrsets.filterAttrs (n: v: v.dnsmasq == true) hps
                )

            )
        );

    };
  };

}
