# demo.nix
{ pkgs, lib, config, ... }:
let
  package = (import ../default.nix).package;
in
with lib;
{
  imports = [
    <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
    <nixpkgs/nixos/modules/virtualisation/qemu-vm.nix>

    ./module_new.nix

  ];

  config = {
    services.qemuGuest.enable = true;

    fileSystems."/" = {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
      autoResize = true;
    };

    boot =
      {
        growPartition = true;
        kernelParams = [ "console=ttyS0" "boot.shell_on_fail" ];
        loader.timeout = 5;
        # Not set because `qemu-vm.nix` overrides it anyway:
        # loader.grub.device = "/dev/vda";
      };

    virtualisation = {
      # only define this, if you want to reuse the vm
      #diskImage = "/mnt/local/data/VM/qemu/dockervm.qcow2";
      diskSize = 1024; # MB

      cores = 1;
      memorySize = 250; # MB
      writableStoreUseTmpfs = false;
      graphics = false;

      #sharedDirectories = {
      #  nixcache = { source = "/mnt/local/data/VM/shared/nix"; target = "/nix"; };
      #};
    };

    # So that we can ssh into the VM, see e.g.
    # http://blog.patapon.info/nixos-local-vm/#accessing-the-vm-with-ssh
    services.openssh.enable = true;
    services.openssh.permitRootLogin = "yes";
    # Give root an empty password to ssh in.
    users.extraUsers.root.password = "";
    users.mutableUsers = false;

    environment.systemPackages = with pkgs; [
      git
      htop
      vim
      nix-diff
    ];


  };

}
 
  
  




