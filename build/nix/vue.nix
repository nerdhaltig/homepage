# nix-shell -E "with import <nixpkgs> {}; callPackage /path/to/package.nix {}"
{ system ? stdenv.hostPlatform.system
, pkgs ? import <nixpkgs> { }
, lib
, stdenv
, git
, yarn
, pname ? "nerdhaltig"
, src
, version
, versionNodeModules
}:
let


  srcYarnIgnorePatterns = ''
    *
    !package.json
    !yarn.lock
  '';
  srcYarnOnly = pkgs.nix-gitignore.gitignoreSourcePure srcYarnIgnorePatterns src;

  defaultYarnOpts = [ "frozen-lockfile" "non-interactive" "no-progress" ];

  yarnCache = stdenv.mkDerivation {
    name = "${pname}-${versionNodeModules}-${system}-yarn-cache";
    src = srcYarnOnly;
    nativeBuildInputs = [ yarn git ];

    buildPhase = ''
      export HOME=$PWD

      yarn config set yarn-offline-mirror $out
      find "$PWD" -name "yarn.lock" -printf "%h\n" | \
        xargs -I {} yarn --cwd {} \
          --frozen-lockfile --ignore-scripts --ignore-platform \
          --ignore-engines --no-progress --non-interactive
    '';
    installPhase = "true";
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";

    # to get hash values use nix-build -A code-server.prefetchYarnCache
    outputHash = {
      x86_64-linux = "sha256-sA0S8bX9Ki86gRy8u/4dmZRSz72xUI1TkjFpD0OHMFs=";
      aarch64-linux = "sha256-sA0S8bX9Ki86gRy8u/4dmZRSz72xUI1TkjFpD0OHMFs=";
      x86_64-darwin = "sha256-sA0S8bX9Ki86gRy8u/4dmZRSz72xUI1TkjFpD0OHMFs=";
    }.${system} or (throw "Unsupported system ${system}");
  };

in
stdenv.mkDerivation {
  pname = pname;
  inherit version;
  inherit src;

  nativeBuildInputs = with pkgs; [ coreutils nodejs yarn ];

  configurePhase = ''
    export HOME=$PWD/tmp
    mkdir -p $HOME

    # run yarn offline by default
    echo '--install.offline true' > .yarnrc

    # set offline mirror to yarn cache we created in previous steps
    yarn --offline config set yarn-offline-mirror "${yarnCache}"
  '';

  buildPhase = ''
    export PATH=node_modules/.bin:$PATH

    ${pkgs.yarn}/bin/yarn install --offline
    patchShebangs node_modules/

    yarn build
  '';

  installPhase = ''
    mv dist $out
  '';
}

